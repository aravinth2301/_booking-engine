package au.com.automic.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import au.com.automic.dto.PromotionType;
import au.com.automic.dto.Tour;

public class PromotionRule {

	private boolean active;

	private String ruleId;

	private PromotionType promotionType;

	private Map<String, Integer> expectedQTY = new HashMap<>();

	private Map<Set<String>, List<Tour>> promotionQTY = new HashMap<>();

	private Map<String, Tour> promotions = new HashMap<String, Tour>();

	public Map<String, Tour> calculate(Map<String, Tour> tours, boolean eligible) {

		Map<String, Tour> tmp = new HashMap<String, Tour>();
		/**
		 * iterating tour promotion eligibility check
		 */
		boolean foundPromotion = false;
		for (String key : tours.keySet()) {
			Integer currentQTY = tours.get(key).getQTY();
			Integer eQTY = expectedQTY.get(key) != null ? expectedQTY.get(key) : 0;
			Integer nextQTY = currentQTY - eQTY;
			eligible = eligible && nextQTY >= 0;
			tmp.put(key, new Tour(key, nextQTY, tours.get(key).getPrice()));
			foundPromotion = foundPromotion || eQTY!=0;
		}
		/**
		 * fetch all Promotions
		 */
		List<Tour> promoTour = promotionQTY.get(expectedQTY.keySet());
		eligible = eligible && foundPromotion;
		
		if (eligible && promoTour != null && !promoTour.isEmpty()) {
			if (PromotionType.DEALS == promotionType) {

				for (Tour tour : promoTour) {
					Integer eQTY = expectedQTY.get(tour.getId());
					Integer qty = eQTY - tour.getQTY();
					if (qty > 0)
						if (promotions.get(tour.getId()) == null) {
							promotions.put(tour.getId(),
									new Tour(tour.getId(), qty, tour.getPrice(), PromotionType.DEALS));
						} else {
							Tour tourTmp = promotions.get(tour.getId());
							tourTmp.setQTY(tourTmp.getQTY() + qty);
						}
				}

			} else if (PromotionType.DISCOUNT == promotionType) {

				for (Tour tour : promoTour) {
					Integer eQTY = tours.get(tour.getId()).getQTY();

					if (promotions.get(tour.getId()) == null) {
						promotions.put(tour.getId(),
								new Tour(tour.getId(), eQTY, tour.getPrice(), PromotionType.DISCOUNT));
					}
				}

			} else if (PromotionType.FREE == promotionType) {
				for (Tour tour : promoTour) {
					Integer eQTY = tour.getQTY();

					if (promotions.get(tour.getId()) == null) {
						promotions.put(tour.getId(),
								new Tour(tour.getId(), eQTY, tour.getPrice(), PromotionType.FREE));
					} else {
						Tour tourTmp = promotions.get(tour.getId());
						tourTmp.setQTY(tourTmp.getQTY() + eQTY);
					}
				}
			}

		}
		if (eligible) {

			return calculate(tmp, eligible);
		}
		return tours;
	}
	
	public Map<String, Integer> getExpectedQTY() {
		return expectedQTY;
	}

	public void setExpectedQTY(Map<String, Integer> expectedQTY) {
		this.expectedQTY = expectedQTY;
	}

	public Map<Set<String>, List<Tour>> getPromotionQTY() {
		return promotionQTY;
	}

	public void setPromotionQTY(Map<Set<String>, List<Tour>> promotionQTY) {
		this.promotionQTY = promotionQTY;
	}

	public void addExpectedQTY(String id, Integer QTY) {
		if (expectedQTY == null) {
			expectedQTY = new HashMap<String, Integer>();
		}
		expectedQTY.put(id, QTY);
	}

	public void addPromotionQTY(List<Tour> deal) {
		if (promotionQTY == null) {
			promotionQTY = new HashMap<Set<String>, List<Tour>>();
		}
		promotionQTY.put(expectedQTY.keySet(), deal);
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public PromotionType getPromotionType() {
		return promotionType;
	}

	public void setPromotionType(PromotionType promotionType) {
		this.promotionType = promotionType;
	}

	public Map<String, Tour> getPromotions() {
		return promotions;
	}

	public void setPromotions(Map<String, Tour> promotions) {
		this.promotions = promotions;
	}

	public String getRuleId() {
		return ruleId;
	}

	public void setRuleId(String ruleId) {
		this.ruleId = ruleId;
	}
}
