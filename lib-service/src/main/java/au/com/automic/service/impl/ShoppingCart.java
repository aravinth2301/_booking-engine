package au.com.automic.service.impl;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import au.com.automic.dto.PromotionType;
import au.com.automic.dto.Tour;
import au.com.automic.dto.TourDetail;
import au.com.automic.util.PromotionRule;

public class ShoppingCart {

	private List<PromotionRule> promotionRules;

	public ShoppingCart(List<PromotionRule> promotionRules) {
		this.promotionRules = promotionRules;
	}

	private Map<String, Tour> tours;

	public void add(String tour, Integer QTY) {
		if (tours == null) {
			tours = new HashMap<String, Tour>();
		}
		if (tours.get(tour) == null) {
			tours.put(tour, new Tour(tour, QTY, TourDetail.get(tour).getPrice()));
		} else {
			Integer qty = tours.get(tour).getQTY();
			tours.get(tour).setQTY(qty + QTY);
		}
	}

	public BigDecimal total() {
		BigDecimal total = BigDecimal.ZERO;
		Map<String, Tour> promotions = new HashMap<String, Tour>();
		for (PromotionRule promotionRule : promotionRules) {
			promotionRule.calculate(tours, true);
			promotions.putAll(promotionRule.getPromotions());
		}

		for (String key : tours.keySet()) {
			Tour tour = tours.get(key);
			total = total.add(tour.getPrice().multiply(BigDecimal.valueOf(tour.getQTY())));
			System.out.println("Tour ID: " + key + " QTY :" + tour.getQTY());
		}

		for (String key : promotions.keySet()) {
			Tour tour = promotions.get(key);
			if (tour.getPromotion() == PromotionType.DEALS) {
				BigDecimal dis = tour.getPrice().multiply(BigDecimal.valueOf(tour.getQTY()));
				total = total.subtract(dis);
				System.out.println("Deal on : " + key + " discount : $" + dis);
			}

			if (tour.getPromotion() == PromotionType.DISCOUNT) {
				BigDecimal dis = tour.getPrice().multiply(BigDecimal.valueOf(tour.getQTY()));
				total = total.subtract(dis);
				System.out.println(key + " discount : $" + dis);
			}

			if (tour.getPromotion() == PromotionType.FREE) {
				System.out.println("free tickets (" + tour.getId() + ") :" + tour.getQTY());
			}

		}
		System.out.println("Total :" + total);
		return total;
	}
}
