package au.com.automic.dto;

import java.math.BigDecimal;

public class Tour {

	private String id;
	private Integer QTY;
	private BigDecimal price;
	private PromotionType promotion;
	
	public Tour() {
		super();
	}
	public Tour(String id, Integer qTY, BigDecimal price) {
		super();
		this.id = id;
		QTY = qTY;
		this.price = price;
	}
	
	public Tour(String id, Integer qTY) {
		super();
		this.id = id;
		QTY = qTY;
	}
	public Tour(String id, Integer qTY, BigDecimal price, PromotionType promotion) {
		super();
		this.id = id;
		QTY = qTY;
		this.price = price;
		this.promotion = promotion;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Integer getQTY() {
		return QTY;
	}
	public void setQTY(Integer qTY) {
		QTY = qTY;
	}
	public PromotionType getPromotion() {
		return promotion;
	}
	public void setPromotion(PromotionType promotion) {
		this.promotion = promotion;
	}
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	
}
