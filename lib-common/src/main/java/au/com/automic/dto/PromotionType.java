package au.com.automic.dto;

public enum PromotionType {

	DISCOUNT,
	DEALS,
	FREE
}
