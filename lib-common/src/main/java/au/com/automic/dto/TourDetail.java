package au.com.automic.dto;

import java.math.BigDecimal;

public enum TourDetail{

	OH("OH","Opera house tour",new BigDecimal(300)),
	BC("BC","Sydney Bridge Climb",new BigDecimal(110),new BigDecimal(20)),
	SK("SK","Sydney Sky Tower",new BigDecimal(30));
	
	private String id;
	private String name;
	private BigDecimal price;
	private BigDecimal discountPrice;

	private TourDetail(String id,String name, BigDecimal price) {
		this.id = id;
		this.name = name;
		this.price = price;
	}
	
	private TourDetail(String id,String name, BigDecimal price,BigDecimal discountPrice) {
		this.id = id;
		this.name = name;
		this.price = price;
		this.discountPrice = discountPrice;
	}

	public static TourDetail get(String id) {
		if("OH".equals(id)) {
			return TourDetail.OH;
		}else if("BC".equals(id)) {
			return TourDetail.BC;
		}
		else
			return TourDetail.SK;
		
	}
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public BigDecimal getDiscountPrice() {
		return discountPrice;
	}

	public void setDiscountPrice(BigDecimal discountPrice) {
		this.discountPrice = discountPrice;
	}
	
	
}
