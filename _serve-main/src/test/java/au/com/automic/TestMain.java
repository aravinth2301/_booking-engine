package au.com.automic;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import au.com.automic.dto.PromotionType;
import au.com.automic.dto.Tour;
import au.com.automic.dto.TourDetail;
import au.com.automic.service.impl.ShoppingCart;
import au.com.automic.util.PromotionRule;

public class TestMain {
	
	private List<PromotionRule> promotionRules = new ArrayList<PromotionRule>();
	
	PromotionRule rule1 = null;
	PromotionRule rule2 = null;
	PromotionRule rule3 = null;
	@Before
	public void init() {
		rule1 = new PromotionRule();
		rule1.setRuleId("R1");
		rule1.addExpectedQTY("OH", 3);
		List<Tour> deal1 = new ArrayList<Tour>();
		deal1.add(new Tour("OH", 2,TourDetail.get("OH").getPrice()));
		rule1.addPromotionQTY(deal1);
		rule1.setPromotionType(PromotionType.DEALS);
		promotionRules.add(rule1);
		
		rule2 = new PromotionRule();
		rule2.setRuleId("R2");
		rule2.addExpectedQTY("OH", 1);
		List<Tour> deal2 = new ArrayList<Tour>();
		deal2.add(new Tour("SK", 1,TourDetail.get("SK").getPrice()));
		rule2.addPromotionQTY(deal2);
		rule2.setPromotionType(PromotionType.FREE);
		promotionRules.add(rule2);
		
		rule3 = new PromotionRule();
		rule3.setRuleId("R3");
		rule3.addExpectedQTY("BC", 5);
		List<Tour> deal3 = new ArrayList<Tour>();
		deal3.add(new Tour("BC", 1,TourDetail.get("BC").getDiscountPrice()));
		rule3.addPromotionQTY(deal3);
		rule3.setPromotionType(PromotionType.DISCOUNT);
		promotionRules.add(rule3);
	}
	
	@Test
	public void promotion1() {
		ShoppingCart sp = new ShoppingCart(promotionRules);
		sp.add("OH",3);
		sp.add("BC",1);
		BigDecimal tot = sp.total();
		assertThat(tot, is(new BigDecimal(710)));
	}
	
	@Test
	public void promotion2() {
		ShoppingCart sp = new ShoppingCart(promotionRules);
		sp.add("OH",1);
		BigDecimal tot = sp.total();
		assertThat(tot, is(new BigDecimal(300)));
	}
	
	@Test
	public void promotion3() {
		ShoppingCart sp = new ShoppingCart(promotionRules);
		sp.add("BC",5);
		sp.add("OH",1);
		BigDecimal tot = sp.total();
		assertThat(tot, is(new BigDecimal(750)));
	}
	
	
	@Test
	public void promotion4() {
		ShoppingCart sp = new ShoppingCart(getPromotion4Rules());
		sp.add("OH",8);
		BigDecimal tot = sp.total();
		assertThat(tot, is(new BigDecimal(2160)));
	}
	
	@Test
	public void promotion4DifferentOrder() {
		ShoppingCart sp = new ShoppingCart(getPromotion4DifferentOrder());
		sp.add("OH",8);
		BigDecimal tot = sp.total();
		assertThat(tot, is(new BigDecimal(1800)));
	}
	
	
	private List<PromotionRule> getPromotion4Rules() {
		List<PromotionRule> rules = new ArrayList<>();
		
		PromotionRule r1 = new PromotionRule();
		/**
		 *  this rule will be ignored
		 */
		List<Tour> d1 = new ArrayList<Tour>();
		r1.addExpectedQTY("OH", 3);
		d1.add(new Tour("OH", 2,TourDetail.get("OH").getPrice()));
		r1.addPromotionQTY(d1);
		r1.setPromotionType(PromotionType.DEALS);
		rules.add(r1);
		
		PromotionRule r2 = new PromotionRule();
		List<Tour> d2 = new ArrayList<Tour>();
		r2.addExpectedQTY("OH", 1);
		d2.add(new Tour("SK", 1,TourDetail.get("SK").getPrice()));
		r2.addPromotionQTY(d2);
		r2.setPromotionType(PromotionType.FREE);
		rules.add(r2);
		
		PromotionRule r3 = new PromotionRule();
		List<Tour> d3 = new ArrayList<Tour>();
		r3.addExpectedQTY("OH", 5);
		d3.add(new Tour("OH", 1,BigDecimal.valueOf(30)));
		r3.addPromotionQTY(d3);
		r3.setPromotionType(PromotionType.DISCOUNT);
		rules.add(r3);
		return rules;
		
	}
	
	
	private List<PromotionRule> getPromotion4DifferentOrder() {
		List<PromotionRule> rules = new ArrayList<>();
		
		/**
		 *  this rule will be ignored
		 */
		PromotionRule r3 = new PromotionRule();
		List<Tour> d3 = new ArrayList<Tour>();
		r3.addExpectedQTY("OH", 5);
		d3.add(new Tour("OH", 1,BigDecimal.valueOf(30)));
		r3.addPromotionQTY(d3);
		r3.setPromotionType(PromotionType.DISCOUNT);
		rules.add(r3);
		
		PromotionRule r1 = new PromotionRule();
		List<Tour> d1 = new ArrayList<Tour>();
		r1.addExpectedQTY("OH", 3);
		d1.add(new Tour("OH", 2,TourDetail.get("OH").getPrice()));
		r1.addPromotionQTY(d1);
		r1.setPromotionType(PromotionType.DEALS);
		rules.add(r1);
		
		PromotionRule r2 = new PromotionRule();
		List<Tour> d2 = new ArrayList<Tour>();
		r2.addExpectedQTY("OH", 1);
		d2.add(new Tour("SK", 1,TourDetail.get("SK").getPrice()));
		r2.addPromotionQTY(d2);
		r2.setPromotionType(PromotionType.FREE);
		rules.add(r2);
		

		return rules;
		
	}
}
